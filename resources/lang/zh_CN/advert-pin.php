<?php
return [
    'labels' => [
        'AdvertPin' => '位置管理',
        'pin' => '位置管理',
    ],
    'fields' => [
        'name' => '名称',
        'flag' => '标识',
        'desc' => '描述',
        'channel' => '栏目',
        'channel_id' => '栏目ID',
        'order' => '排序',
    ],
    'options' => [
    ],
];
