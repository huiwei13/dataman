<?php
return [
    'labels' => [
        'Notice' => '通知公告',
        'notice' => '通知公告',
    ],
    'fields' => [
        'title' => '标题',
        'from' => '来源',
        'status' => '状态',
        'top' => '置顶',
        'content' => '内容',
    ],
    'options' => [
    ],
];
